<!-- fg=green bg=black -->
<!--effect=matrix-->
# SQL Injection
## Group 2

@milkbread     @iKool     @5y4mp

---

## SQL Injection ??

- Sebuah kelemahan pada laman web bagi membolehkan hacker bermain dengan queries yang dibuat dalam sesuatu aplikasi untuk masuk dalam pangkalan data (database).
- Teknik serangan mengeksploitasi keselamatan dalam pangkalan data.
- SQL injection adalah teknik injection code yang boleh memusnahkan database seseorang.

<!-- fg=green bg=black -->
![codio](anom1.yml)


---

## Teknik Menyerang 
<!-- fg=green bg=black -->

```
Hacker
     \
      \ 
       \
        \
         \
          ---------------------------------------------------------------------------------------------------------------
         (Laman Web)---> Menentukan skema database --> Mengekstrak data --> Menambah/Mengubah data --> Memintas pengesahan
          ---------------------------------------------------------------------------------------------------------------
         / 
        /
       /
      / 
     /
Hacker 
```

Teknik Penyerangan (Image) [link](https://miro.medium.com/max/600/1*mTKfq-BXgewrnF8A57TTNw.png)
Teknik Penyerangan (Demo) [link](https://drive.google.com/file/d/1I5Ft9r8T5qME2wJ0mLVEjQ8ZIQnRwTXJ/view?usp=sharing)

---

<!-- fg=green bg=black-->

## Bagaimana SQL injection berfungsi

1. Keupayaan untuk memasukkan arahan SQL ke dalam enjin pangkalan data melalui aplikasi sedia ada .

2. Memasukkan SQL ke dalam form(Login).

3. Mengenalpasti kelemahan dengan sengaja memberi input yang salah sebagai titik masuk ke pangkalan data.

4. Aplikasi menghantar SQL Query kepada DB Server.

5. DB melaksanakan query termasuk mengeksploitasi dan menghantar data kembali ke aplikasi.

6. Aplikasi kembalikan data kepada pengguna.


---

<!-- fg=green bg=black -->
## Kesan Daripada SQL Injection

+-----------------------------------+
|                                   |
| Kebocoran maklumat sensitif       |
| Reputasi merosot                  |
| Pengubahsuaian maklumat sensitif  |
| Kehilangan kawalan DB Server      |
| Kehilangan data                   |
| Penafian perkhidmatan             |
|                                   |
|                                   |
|                                   |
+-----------------------------------+ 


---

## Pertahanan Terhadap SQL Injection 

1. Pembersihan data yang komprehensif
2. Menggunakan aplikasi web Firewall
3. Hadkan keistimewaan pangkalan data mengikut konteks
4. Elakkan membina queries SQL dengan input pengguna

---

# Jenis SQL Injection


   Jenis Injection                           Alat SQL Injection 

+-------------------------------+       +---------------------------+
|                               |       |                           |
| Shell Injection               |       |  BSQL Hacker              |
| Scripting language injection  |       |  SQLmap                   |
| File Inclusion                |       |  SQLninja                 |
| XML Injection                 |       |  Safe3 SQL Injector       |
| XPath Injection               |       |  SQLSus                   |
| LDAP Injection                |       |  Mole                     |
| SMTP Injection                |       |  Havij                    |
|                               |       |                           |
|                               |       |                           |
+-------------------------------+       +---------------------------+



---

<!-- fg=green bg=black -->
## Konklusi

- SQL injection ialah teknik untuk mengeksploitasi aplikasi yang menggunakan pangkalan data hubungan sebagai hujung belakangnya. 

- Aplikasi mengarang pernyataan SQL dan menghantar ke pangkalan data.  

- Teknik ini berdasarkan malformed user-supplied data.  

- Menyebabkan akses tanpa kebenaran, pemadaman data atau kecurian maklumat

- Semua pangkalan data boleh menjadi sasaran SQL injection dan semuanya boleh dirawat dengan teknik ini.

- Vulnerability yang berada di dalam lapisan aplikasi di luar pangkalan data  menyebabkan aplikasi mempunyai sambungan ke pangkalan data. 

---


<!--effect=stars-->

# SQL Injection
## Group 2

@milkbread     @iKool     @5y4mp
---
